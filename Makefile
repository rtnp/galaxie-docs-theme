.PHONY: help install-python venv-create venv-requirements prepare docs tests docs clean
#SHELL=/bin/bash

#!make
include .env
export $(shell sed 's/=.*//' .env)

help:
	@echo  'CleaningUp:'
	@echo  '  clean               - Remove every created directory and restart from scratch'
	@echo  ''
	@echo  'Documentations:'
	@echo  '  docs                - Install Sphinx and requirements, then build documentations'
	@echo  ''
	@echo  'Preparation:'
	@echo  '   prepare            - Easy way for make everything'
	@echo  ''
	@echo  'Tests:'
	@echo  '   tests              - Easy way for make tests over a CI'

install-python:
	@echo 'INSTALL PYTHON'
	apt-get update && apt install -y python3 python3-pip python3-venv

header:
	@echo "************************** GALAXIE DOCS THEME MAKEFILE *************************"
	@echo "HOSTNAME	`uname -n`"
	@echo "KERNEL RELEASE `uname -r`"
	@echo "KERNEL VERSION `uname -v`"
	@echo "PROCESSOR	`uname -m`"
	@echo "********************************************************************************"

prepare: header
	@ if test -d $(DEV_VENV_DIRECTORY);\
	then echo "[  OK  ] VIRTUAL ENVIRONMENT";\
	else echo "[CREATE] VIRTUAL ENVIRONMENT" &&\
		cd $(DEV_DIRECTORY) && python3 -m venv $(DEV_VENV_NAME);\
	fi

	@${DEV_VENV_ACTIVATE} && pip3 install -U pip --no-cache-dir --quiet &&\
	echo "[  OK  ] PIP3" || \
	echo "[FAILED] PIP3"

	@${DEV_VENV_ACTIVATE} && pip3 install -U wheel --no-cache-dir --quiet &&\
	echo "[  OK  ] WHEEL" || \
	echo "[FAILED] WHEEL"

	@${DEV_VENV_ACTIVATE} && pip3 install -U setuptools --no-cache-dir --quiet &&\
	echo "[  OK  ] SETUPTOOLS" || \
	echo "[FAILED] SETUPTOOLS"

	@ echo ""
	@ echo "************************************ INSTALL ***********************************"
	@ ${DEV_VENV_ACTIVATE} &&\
	python -m pip install -e .

tests:
	@echo 'RUN TESTS'
	@ ${DEV_VENV_ACTIVATE} && python setup.py green -r

clean: header
	@ echo ""
	@ echo "*********************************** CLEAN UP ***********************************"
	rm -rf ./venv
	rm -rf ~/.cache/pip
	rm -rf ./.eggs
	rm -rf ./*.egg-info
	rm -rf .coverage

docs: prepare
	@ echo ""
	@ echo "***************************** BUILD DOCUMENTATIONS *****************************"
	@ ${DEV_VENV_ACTIVATE} &&\
  	pip3 install -r docs/requirements.txt --no-cache-dir --quiet &&\
	cd $(DEV_DIRECTORY)/docs &&\
	make html