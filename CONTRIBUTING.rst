==============
How to release
==============

* Update CHANGELOG.rst
* Bump version (YYYY.MM) in setup.py and python_docs_theme/theme.conf
* Commit
* Push to check one last time if the tests pass github side.
* Tag it (YYYY.MM).
* Build (``python -m build``)
* Test it (in :file:`cpython/Doc` run
  ``./venv/bin/pip install ../../python-docs-theme/dist/python-docs-theme-2021.8.tar.gz``
  then build the doc using ``make html``).
* Upload it: ``twine upload dist/*``.
* Bump version (YYYY.MM.dev) in setup.py and python_docs_theme/theme.conf
* Commit this last bump.
* Push and push the tag (``git push && git push --tags``)
