.. title:: Galaxie Docs Theme

================================
Galaxie Docs Theme Documentation
================================
.. code:: text

                  ________        __                 __
                 /  _____/_____  |  | _____  ___  __|__| ____
                /   \  ___\__  \ |  | \__  \ \  \/  /  |/ __ \
                \    \_\  \/ __ \|  |__/ __ \_>    <|  \  ___/_
                 \________(______/____(______/__/\__\__|\_____/


Actual Homepage : https://gitlab.com/rtnp/galaxie-docs-theme

Documentation : https://galaxie-docs-theme.readthedocs.io

The Project
-----------
*Galaxie Docs* Theme is *Sphinx* theme , dedicated to ultra low device.
The theme is optimised for TTY Browser like *links2*, *lynx*, *elinks*, etc...

In case of outage, each octet can be counted, then you should consider low profile computer or tools.

The Mission
-----------
* Provide a Theme for text based browser
* Permit to ultra low computer to read online pages
* Easily installable with *pip*
* Coupled with *Sphinx* have capability to manage a web portal for low profile computer's
* Permit text based logo system

Features
--------
* Each char is count
* Simple HTML
* Minimal JavaScript (Without that it will be better)
* Minimal CSS (Without that it will be better)

Contribute
----------
* Issue Tracker: https://gitlab.com/rtnp/galaxie-docs-theme/issues
* Source Code: https://gitlab.com/rtnp/galaxie-docs-theme

Our collaboration model is the Collective Code Construction Contract (C4): https://rfc.zeromq.org/spec:22/C4/


License
-------
GNU GENERAL PUBLIC LICENSE Version 3


See the LICENCE_

.. _LICENCE: https://gitlab.com/rtnp/galaxie-docs-theme/blob/master/LICENSE.rst

All contributions to the project source code ("patches") SHALL use the same license as the project.

Indices and tables
------------------
* :ref:`genindex`
* :ref:`search`