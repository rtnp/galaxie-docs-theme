=========
Changelog
=========

`2021.11 <https://github.com/python/python-docs-theme/releases/tag/2021.11>`_
---------------------------------------------------------------------------

Initial release.